/* AgriTech is IoT device for Agriventor Competition
 *  we are develop auotomatization in farming process
 *  created by: Muhammad Hilmi Asyrofi, ComputerScience ITB
 *             Hibatul Ghazi Zulhasmi, Electrical Engineering UGM
 */

 
#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP8266.h>

// Define ESP8266 Pin
// Mapping to Arduino IDE
#define D0 16
#define D1 5
#define D2 4
#define D3 0
#define D4 2
#define D5 14
#define D6 12
#define D7 13
#define D8 15
#define D9 3
#define D10 1

// WiFi network info.
char ssid[] = "mh";
char wifiPassword[] = "abcdefgh";

// Cayenne authentication info. This should be obtained from the Cayenne Dashboard.
char username[] = "23202e80-f83b-11e7-939f-b354e6d68235";
char password[] = "68404029617c2209cc269cecd15f836eac49f033";
char clientID[] = "5be4d040-f84a-11e7-8934-ff70c6ef636b";

/*------ PIN Motor Driver ------*/
int CHANNEL_MOTOR = 4;
int PIN_ENABLE_A = D5;
int PIN_IN_1 = D3;
int PIN_IN_2 = D4;
int pwm;

/*------ PIN Relay------*/
int CHANNEL_PENYIRAM = 2;
int CHANNEL_HORMON = 3;
int PIN_RELAY_PENYIRAM = D1;
int PIN_RELAY_HORMON = D2;
int relay_penyiram;
int relay_hormon;

/*------ PIN Moisture ------*/
int PIN_MOISTURE = A0;
int CHANNEL_MOISTURE = 1;
int low_threshold_moisture;
int high_threshold_moisture;
int val_moisture;

void setup() {
  Serial.begin(9600);
  Cayenne.begin(username, password, clientID, ssid, wifiPassword);
  pwm = 0;
  relay_penyiram = 0;
  relay_hormon = 0;

  pinMode(PIN_RELAY_PENYIRAM, OUTPUT);
  pinMode(PIN_RELAY_HORMON, OUTPUT);

  pinMode(PIN_ENABLE_A, OUTPUT);
  pinMode(PIN_IN_1, OUTPUT);
  pinMode(PIN_IN_2, OUTPUT);
  
  digitalWrite(PIN_IN_1, LOW);
  digitalWrite(PIN_IN_2, HIGH);
  analogWrite(PIN_ENABLE_A, 0);
  
}

void loop() {
  Cayenne.loop();

  //Move peristaltic pump
  analogWrite(PIN_ENABLE_A, pwm);  
  digitalWrite(PIN_IN_1, LOW);
  digitalWrite(PIN_IN_2, HIGH);

  // terbalik
  if (relay_penyiram == LOW) {
    digitalWrite(PIN_RELAY_PENYIRAM, HIGH);
  } else {
    digitalWrite(PIN_RELAY_PENYIRAM, LOW);
  }


  // terbalik
  if (relay_hormon == LOW) {
    digitalWrite(PIN_RELAY_HORMON, HIGH);
  } else {
    digitalWrite(PIN_RELAY_HORMON, LOW);
  }

}

// Default function for sending sensor data at intervals to Cayenne.
// You can also use functions for specific channels, e.g CAYENNE_OUT(1) for sending channel 1 data.
CAYENNE_OUT_DEFAULT()
{
  // Write data to Cayenne here. This example just sends the current uptime in milliseconds on virtual channel 0.
  val_moisture = analogRead(PIN_MOISTURE);
  
  // Then, we will map the output values to 0–100, because the moisture is measured in percentages. 
  // When we took the readings from the dry soil, the sensor value was 550, 
  // and in the wet soil, the sensor value was 10. 
  val_moisture /= 10; // nilai moisture dalam persen
  val_moisture = 100 - val_moisture;
//  val_moisture = map(val_moisture,550,10,0,100);
  Serial.print("Moisture: ");
  Serial.println(val_moisture);
  Cayenne.virtualWrite(CHANNEL_MOISTURE, val_moisture);
  if (val_moisture < low_threshold_moisture) {
    Cayenne.virtualWrite(2, HIGH);
    relay_penyiram = 1;
  }
  if (val_moisture > high_threshold_moisture) {
    Cayenne.virtualWrite(2, LOW);
    relay_penyiram = 0;
  }
}

// Default function for processing actuator commands from the Cayenne Dashboard.
// You can also use functions for specific channels, e.g CAYENNE_IN(1) for channel 1 commands.
CAYENNE_IN_DEFAULT()
{
  CAYENNE_LOG("Channel %u, value %s", request.channel, getValue.asString());
//  Serial.println("AAAAA");
  //Process message here. If there is an error set an error message using getValue.setError(), e.g getValue.setError("Error message");
}

// Read condition on/off from cayenne Dashboard
CAYENNE_IN(2)
{
  // get value sent from dashboard
  int condition = getValue.asInt(); // 0 to 1
  Serial.print ("Penyiram Condition: ");
  Serial.println(condition);
  relay_penyiram = condition;
  
} 

// Read condition on/off from cayenne Dashboard
CAYENNE_IN(3)
{
  // get value sent from dashboard
  int condition = getValue.asInt(); // 0 to 1
  Serial.print ("Hormon Condition: ");
  Serial.println(condition);
  relay_hormon = condition;
  
}

// Read pwm from cayenne Dashboard
CAYENNE_IN(4)
{
  // get value sent from dashboard
  int motorPWMValue = getValue.asInt(); // 0 to 1
  Serial.print ("Motor Driver Controller: ");
  Serial.println(motorPWMValue);
  pwm = motorPWMValue; //set pwm value
  
}


// Read low threshold for moisture from cayenne Dashboard
CAYENNE_IN(5)
{
  // get value sent from dashboard
  int low = getValue.asInt(); // 0 to 1
  Serial.print ("Low Threshold Moisture: ");
  Serial.println(low);
  low_threshold_moisture = low; //set threshold value
  
}


// Read the hight sensor for moisture from cayenne Dashboard
CAYENNE_IN(6)
{
  // get value sent from dashboard
  int high = getValue.asInt(); // 0 to 1
  Serial.print ("High Threshold Moisture: ");
  Serial.println(high);
  high_threshold_moisture = high; //set pwm value
  
}


